KextExcludeListUpdater - the manager for AppleKextExcludeList.kext
======================

KextExcludeListUpdater is a program for the managing of the list in AppleKextExcludeList.kext.

KextExcludeListUpdater is licensed under a **MIT** .

* Website: <https://bitbucket.org/marchrius/kextexcludelistupdater>
* Usage guide: <https://bitbucket.org/marchrius/kextexcludelistupdater/wiki/Home>

What It Can Do
==================================

* Add an identifier and version of one kext (manually).
* Add all the identifiers and version in a directory (automatically). If the kext have a PlugIns directory it add alt he se sub-kext.
* Remove one identifier and version. 
* Can update itself! You don't need to surf the web to reach the zip file, download, extract and replace the old version! :)


Language Localization
==================================

The localization of the program is English. If you want to help me with localization read below.

How Can I Contribute
==================================

Fork KextExcludeListUpdater, add your improvement, push it to a branch
in your fork named for the topic, send a pull request.

License
==================================
KextExcludeListUpdater is under **MIT**.

Thanks to
==================================
* Haxx for libcurl <http://www.haxx.se/done-curl.html>
* Sam Soffes for SSZipArchive <https://github.com/soffes/ssziparchive>

See the LICENSE file for the full license text.