//
//  main.m
//  KextExcludeListUpdater
//
//  Created by Matteo Gaggiano on 16/10/13.
//  Copyright (c) 2013 Marchrius. All rights reserved.
//
#include <Foundation/Foundation.h>
#include <pwd.h>
#include <curl/curl.h>
#include <libgen.h>
#include "SSZipArchive.h"

#define kIdentifier     @"CFBundleIdentifier"
#define kVersion        @"CFBundleVersion"
#define kFilePath       @"File Path"
#define kExceptionList  @"OSKextSigExceptionList"

static int verbose = 0;
static int delete = 0;
static int silent = 0;
static int yesall = 0;
static char* program_name = 0;
NSAutoreleasePool *pool;    

void print_help();
BOOL isRootUser();
void print_license();
void loadBar(NSUInteger a, NSUInteger b, NSUInteger  c, NSUInteger  d);
void dealloc();
size_t writecallback(void *, size_t, size_t, void *);
NSArray* checkVersion();
NSData* downloadUpdate(NSString*);
void printErrorIfThereIsOne(NSError**);

void dealloc()
{
    if (program_name)
        free(program_name);
    
    program_name = NULL;
    [pool drain];
    VLOGS("Deallocated all variables");
}

int main(int argc, const char * argv[])
{
    pool = [[NSAutoreleasePool alloc] init];    
        
    atexit(dealloc);
    program_name = basename((char*)argv[0]);
        
    if ((argc <= 2 && argc > 1) && (argv[1][0] == '-' && argv[1][1] == 'h')) {
        print_help();
        exit(EXIT_SUCCESS);
        
    } else if (argc == 1) {
        print_help();
        exit(EXIT_SUCCESS);
    }
    
    if ((argc <= 2 && argc > 1) && (argv[1][0] == '-' && argv[1][1] == '-' && argv[1][2] == 'v' && argv[1][3] == 'e' && argv[1][4] == 'r')) {
        LOGS("%s version is %s (%d)", program_name, SHORTVERSION, VERSION);
        exit(EXIT_SUCCESS);
    }
    if ((argc <= 2 && argc > 1) && (argv[1][0]=='-' && argv[1][1] == 'l')) {
        print_license();
        exit(EXIT_SUCCESS);
    }
    
    if ((argc <= 2 && argc > 1) && argv[1][0] == '-' && argv[1][1] == 'u') {
        NSArray* progVersion = [NSArray arrayWithArray:checkVersion()];
        
        if (progVersion.count == 1) {
            ERRLOGS("A problem occured when downloading the information for update. Retry later.\nError code for Curl: %s", [[progVersion objectAtIndex:0] UTF8String]);
            exit(EXIT_FAILURE);
        }
        
        int lastVersion = atoi([[progVersion objectAtIndex:0] UTF8String]);
        
        if (lastVersion > VERSION) {
            LOGS("A new version %s (%s) is available. You have %s (%d).\n%s\n%s", [[progVersion objectAtIndex:1] UTF8String], [[progVersion objectAtIndex:0] UTF8String], SHORTVERSION, VERSION, [[progVersion objectAtIndex:3] UTF8String], [[progVersion objectAtIndex:4] UTF8String]);
            char answer = '\0';
            do {
                LOG("Install it? [y|Y to continue, other to abort] > ");
                
                if (!yesall)
                    scanf("%c", &answer);
                else
                    answer = 'y';
                
            } while (answer == '\0');
            
            switch (answer) {
                case 'Y':
                case 'y':
                    break;
                default:
                    LOGS("Aborted from user. Exiting.");
                    exit(EXIT_SUCCESS);
            }
            
            NSError* errorZipFile = nil;
            NSData*zipFile = downloadUpdate([progVersion objectAtIndex:2]);
            NSString* zipName = [[progVersion objectAtIndex:2] lastPathComponent];
            
            [zipFile writeToFile:[NSString stringWithFormat:@"/tmp/%@", zipName] options:NSDataWritingAtomic error:&errorZipFile];
            
            printErrorIfThereIsOne(&errorZipFile);
            
            NSString* pathZipExtraction = [NSString stringWithUTF8String:dirname((char*)argv[0])];
            
            [SSZipArchive unzipFileAtPath:[NSString stringWithFormat:@"/tmp/%@", zipName] toDestination:pathZipExtraction overwrite:YES password:nil error:&errorZipFile];
            
            printErrorIfThereIsOne(&errorZipFile);

            [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"/tmp/%@", zipName] error:&errorZipFile];
            
            printErrorIfThereIsOne(&errorZipFile);
            
            BOOL isDirectory = YES;
            if ([[NSFileManager defaultManager] fileExistsAtPath:[pathZipExtraction stringByAppendingPathComponent:@"__MACOSX"] isDirectory:&isDirectory]) {
                [[NSFileManager defaultManager] removeItemAtPath:[pathZipExtraction stringByAppendingPathComponent:@"__MACOSX"] error:&errorZipFile];
                
                printErrorIfThereIsOne(&errorZipFile);
            }
            
            LOGS("Updated successfully.");
            
            exit(EXIT_SUCCESS);
        } else {
            LOGS("%s %s (%d)\nYou have the last version available for this program.", program_name, SHORTVERSION, VERSION);
            exit(EXIT_SUCCESS);
        }
    }

    if (!isRootUser()) {
        ERRLOGS("%s: You must run this program with root permissions.", program_name);
        exit(EXIT_FAILURE);
    }
    
    NSString *directoryPath, *infoPath, *kextPath, *version, *name;
    directoryPath = infoPath = kextPath = version = name = nil;
    NSMutableDictionary *infoDict = nil;
    
    int index;
    int c;
    
    opterr = 0;
    while ((c = getopt (argc, (char**)argv, "f:k:n:v:Vydsu")) != -1)
        switch (c)
    {
        case 's':
            silent = 1;
            break;
        case 'd':
            delete = 1;
            break;
        case 'V':
            verbose = 1;
            break;
        case 'y':
            yesall = 1;
            break;
        case 'u':
            break;
        case 'i':
            infoPath = [[NSString stringWithFormat:@"%s", optarg] autorelease];
            break;
        case 'k':
            kextPath = [[NSString stringWithFormat:@"%s", optarg] autorelease];
            break;
        case 'f':
            directoryPath = [[NSString stringWithFormat:@"%s", optarg] autorelease];
            break;
        case 'v':
            version = [[NSString stringWithFormat:@"%s", optarg] autorelease];
            break;
        case 'n':
            name = [[NSString stringWithFormat:@"%s", optarg] autorelease];
            break;
        case '?':
            if (optopt == 'f'  || optopt == 'k'  || optopt == 'v'  || optopt == 'n') {
                ERRLOG("Error: option -%c requires an argument.\n", optopt);
                
            } else if (isprint (optopt)) {
                ERRLOG("Error: unknown option `-%c'.\n", optopt);
                print_help();
                exit(EXIT_FAILURE);
            } else
                ERRLOG("Error: unknown option character `\\x%x'.\n", optopt);
            exit(EXIT_FAILURE);
        default:
            dealloc();
            abort ();
    }
    
    if (silent) {
        verbose = 0;
    }
    
    BOOL exitFlag = false;
    
    for (index = optind; index < argc; index++)
    {
        LOGS("Argument not valid %s", argv[index]);
        if (!exitFlag)
            exitFlag = true;
    }
        
    if (!kextPath) {
        ERRLOGS("Error: path to AppleKextExcludeList.kext not set!");
        exitFlag = true;
    }
    
    if ((!version && name)) {
        ERRLOGS("Error: -v is required if name is set!");
        exitFlag = true;
    }
    
    if ((version && !name)) {
        ERRLOGS("Error: -n option is required if -v is set!");
        exitFlag = true;
    }
    
    if (!directoryPath && !version) {
        ERRLOGS("Error: You must set a -f option or -n and -v option");
        exitFlag = true;
    }
    
    if (directoryPath && version) {
        ERRLOGS("Error: You can't use -n and -v with -f option");
        exitFlag = true;
    }
    
    if (silent && !yesall)
    {
        ERRLOGS("Error: if you set -s option -y is required");
        exitFlag = true;
    }
    
    if (delete && directoryPath) {
        ERRLOGS("Error: If the -f option is set you CAN'T use the -d option");
        exitFlag = true;
    }
    if (delete && !version) {
        ERRLOGS("Error: If the -d is set the -v and -n are both required");
        exitFlag = true;
    }
    

    
    if (exitFlag) {
        print_help();
        exit(EXIT_FAILURE);
    }
    
    exitFlag = false;
    infoPath = [[kextPath stringByAppendingPathComponent:@"Contents"] stringByAppendingPathComponent:@"Info.plist"];
    
    if (!(infoDict = [[NSMutableDictionary alloc] initWithContentsOfFile:infoPath])) {
        ERRLOGS("Error: I can't access to %s dictionary", [infoPath UTF8String]);
        exit(EXIT_FAILURE);
    }
    
    
    NSError* error = nil;
    NSMutableArray* filesArray = [[NSMutableArray new] autorelease];
    NSMutableDictionary*exceptList = [infoDict objectForKey:kExceptionList];
    NSDictionary* tmpDict;
    NSString* fullPath, *tempString;
    NSUInteger i = 0, isAdded = 0, isUpdated = 0;
    NSNumber *vers1, *vers2;
    unsigned long vers1l = 0, vers2l = 0;
    
    
    if (name && version && !delete) {
        if (![exceptList objectForKey:name])
            isAdded++;
        else
            isUpdated++;
        
        [exceptList setObject:version forKey:name];
    } else if (name && version && delete) {
        if (![exceptList objectForKey:name]) {
            LOGS("The %s identifier doen't exist. Nothing to do here.", [name UTF8String]);
            exit(EXIT_SUCCESS);
        } else {
            [exceptList removeObjectForKey:name];
        }
    }

    if (directoryPath) {
    
        VLOGS("Operatin on directory %s", [directoryPath UTF8String]);
        NSArray*tmpArray = [NSArray arrayWithArray:[[NSFileManager defaultManager] subpathsOfDirectoryAtPath:directoryPath error:&error]];
        
        printErrorIfThereIsOne(&error);

        int countInfo = 0;

        for (int k = 0; k < tmpArray.count; k++) {
            if ([[tmpArray objectAtIndex:k] rangeOfString:@"Info.plist"].length > 0) {
                tmpDict = nil;
                tmpDict = [NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", directoryPath, [tmpArray objectAtIndex:k]]];
                if ([tmpDict objectForKey:kIdentifier] && [tmpDict objectForKey:kVersion]) {
                    countInfo++;
                    [filesArray addObject:[tmpArray objectAtIndex:k]];
                }
            }
        }
        
        VLOGS("Found %d Info.plist", countInfo);
        
        VLOGS("Reading properties for each file in %s", [directoryPath UTF8String]);
        
        LOGS("Please wait while populating new ExcludeList...  \n");
        
        i = 0;
        
        for (NSString* file in filesArray) {
            
            if (!verbose && !silent)
                loadBar(i+1, filesArray.count, 100, 70);
            
            fullPath = [NSString stringWithFormat:@"%@/%@", directoryPath, file];
            VLOGS("Working on: %s", [fullPath UTF8String]);

            tmpDict = nil;
            tmpDict = [[[NSDictionary alloc] initWithContentsOfFile:fullPath] autorelease];
            
            if (!tmpDict)
            {
                ERRLOGS("Error: error occurred while reading %s file", [fullPath UTF8String]);
                exit(EXIT_FAILURE);
            }
            
            if (!fullPath && ![tmpDict objectForKey:kIdentifier] && ![tmpDict objectForKey:kVersion]) {
                ERRLOGS("Error: error while generating the file properties.");
                exit(EXIT_FAILURE);
            }
            
            vers1 = nil;
            vers2 = nil;
            
            if ([exceptList objectForKey:[tmpDict objectForKey:kIdentifier]]) {
                
                tempString = [[exceptList objectForKey:[tmpDict objectForKey:kIdentifier]] stringByReplacingOccurrencesOfString:@"." withString:@""];
                
                vers1l = atol([tempString UTF8String]);
                
                vers1 = [NSNumber numberWithUnsignedLong:vers1l];
                
            }
            
            tempString = [[tmpDict objectForKey:kVersion] stringByReplacingOccurrencesOfString:@"." withString:@""];
            
            vers2l = atol([tempString UTF8String]);
            
            vers2 = [NSNumber numberWithUnsignedLong:vers2l];
            
            if (![exceptList objectForKey:[tmpDict objectForKey:kIdentifier]] && vers2) {
                VLOGS("To add: %s version %s (%ld)", [[tmpDict objectForKey:kIdentifier] UTF8String], [[tmpDict objectForKey:kVersion] UTF8String], [vers2 longValue]);
                [exceptList setObject:[tmpDict objectForKey:kVersion]
                               forKey:[tmpDict objectForKey:kIdentifier]];
                isAdded++;
            } else if (vers1 && vers2 && [vers2 isGreaterThan:vers1]) {
                VLOGS("To update: %s version %s (%ld)", [[tmpDict objectForKey:kIdentifier] UTF8String], [[tmpDict objectForKey:kVersion] UTF8String], [vers2 longValue]);
                [exceptList setObject:[tmpDict objectForKey:kVersion]
                               forKey:[tmpDict objectForKey:kIdentifier]];
                isUpdated++;
            }
            
            i++;
        }
    }
    
    if (isAdded > 0 && isUpdated == 0) {
        LOGS("Added %ld identifiers", (long)isAdded);
    }
    if (isUpdated > 0 && isAdded == 0) {
        LOGS("Updated %ld identifiers", (long)isUpdated);
    }
    if (isUpdated > 0 && isAdded > 0) {
        LOGS("Added %ld identifiers and updated %ld identifiers", (long)isAdded, (long)isUpdated);
    }
    if (isAdded == 0 && isUpdated == 0 && !delete) {
        LOGS("Is all up to date");
        exit(EXIT_SUCCESS);
    }
    
    // add the updated dictionary of identifier
    [infoDict setObject:exceptList forKey:kExceptionList];
    
    char response = 0;
    
    do {
        LOG("Would you like to write the new values into Info.plist? (You must have launched this script with root persmissions) [y|Y to continue other to abort] > ");
        
        if (!yesall)
            scanf("%c", &response);
        else
            response = 'y';
        
    } while (response == '\0');
    
    switch (response) {
        case 'Y':
        case 'y':
            break;
        default:
            LOGS("\nAborted from user. Exiting.");
            exit(EXIT_SUCCESS);
    }
    
    if (![infoDict writeToFile:infoPath atomically:YES])
    {
        ERRLOGS("Error: error while writing the new Info.plist in position %s", [infoPath UTF8String]);
        exit(EXIT_FAILURE);
    }
    else
    {
        LOGS("\n%s writed correctly!", [infoPath UTF8String]);
        exit(EXIT_SUCCESS);
    }
    
    return 0;
}

void print_help()
{
    LOGS("Usage example: %s -f /folder/with/kexts/to/add/to/excludelist -k /path/to/AppleKextExcludeList.kext\n\t-f\t\tFolder containing kexts to add to exclude list (Conflict with -v and -n)\n\t-k\t\tPath to AppleKextExcludeList.kext (Required)\n\t-v\t\tSet the version of the kext to add manually to list (Required if -n is set. Conflict with -f)\n\t-n\t\tSet the identifier of the kext to add manually to list (Required if -v is set. Conflict with -f)\n\t-d\t\tDelete the record in Infol.plist with the given -n and -v options (Require -v and -n options)\n\t-y\t\tAnswer automatically at all questions with \"y\"\n\t-V\t\tPrint verbose output\n\t-s\t\tNo output. Errors is printed (Ignore -V option. Require -y option)\n\t-l\t\tPrint license text\n\t-u\t\tChecks for updates\n\t--ver\t\tPrint version\n\t-h\t\tPrint this help message.\n\nCopyright (c) %d %s for Insanelymac\n%s: v%s (%d)", program_name, CURRENTYEAR, DEVELOPER, program_name, SHORTVERSION, VERSION);
}

BOOL isRootUser()
{
        return (getpwuid(geteuid())) && (getpwuid(geteuid())->pw_uid == 0 || getpwuid(geteuid())->pw_gid == 0) ? true : false;
    return false;
}

void print_license()
{
    LOGS(LICENSE, CURRENTYEAR, DEVELOPER);
}

void loadBar(NSUInteger a, NSUInteger b, NSUInteger  c, NSUInteger  d)
{
    if ( a % (b/c) != 0 ) return;
    float ratio = (float)a/(float)b;
    NSUInteger   e     = (ratio * d);
    
    printf("%s", (ratio < 0.009) ? "[ 0%" : "[");
    
    for (NSUInteger x=0; x<e; x++)
        if (x < e - 1)
            printf("%s", (x < e - 2) ? "=" : ">");
        else 
            printf("%3.0f%%", (ratio*100) );
    
    for (NSUInteger x=e; x<d-1; x++)
        printf(" ");
    
    printf("]\n");
    
    if (a < b-2)
        printf("\033[F\033[J");
    /*              ^  ^  ^  ^
                    |  |  |  |___ Comando che elimina la linea successiva
                    |  |  |______ Prossimo è un comando
                    |  |_________ Comando che sale di una linea (Intera linea)
                    |____________ Prossimo è un comando
     */
}

NSArray* checkVersion()
{
    CURL* curl;
    CURLcode res;
    NSMutableData *contentFile = [[NSMutableData new] autorelease];
    char *url = "http://marchrius.altervista.org/KextExcludeListUpdater.txt";
    
    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url);
        
        /* Con questa istruzione scriviamo i dati catturati dall'url in un NSMutableData*/
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, contentFile);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writecallback);
        
        res = curl_easy_perform(curl);
        
        /* always cleanup */
        curl_easy_cleanup(curl);
    }
    
    if (res > 0)
        return @[[NSString stringWithFormat:@"%d", res]];
    
    NSString *version = [[NSString alloc] initWithData:contentFile encoding:NSUTF8StringEncoding];
    
    if (!version) return nil;
    
    if ([version componentsSeparatedByString:STRING_VERSION_SEPARATOR].count != 5) {
        LOGS("Error in the server's file. Contact the Developer.");
        exit(EXIT_FAILURE);
    }

    return [version componentsSeparatedByString:STRING_VERSION_SEPARATOR];
}

NSData* downloadUpdate(NSString* link)
{
    NSError*error = nil;
    NSMutableData *zipData = [[NSMutableData alloc] initWithContentsOfURL:[NSURL URLWithString:link] options:0 error:&error];
    
    printErrorIfThereIsOne(&error);
    
    return zipData;
}

size_t writecallback(void *buffer, size_t size, size_t nmemb, void *stream)
{
    [(NSMutableData*)stream appendBytes:buffer length:size*nmemb];
    
    size_t written = size*nmemb;

   	return written;
}

void printErrorIfThereIsOne(NSError** error)
{
    if (*error)
    {
        ERRLOGS("Error: %s", [[*error localizedDescription] UTF8String]);
        if ([*error localizedRecoverySuggestion]) {
            ERRLOGS("Suggestion to recover the error: %s", [[*error localizedRecoverySuggestion] UTF8String]);
        }
        exit(EXIT_FAILURE);
    } else {
        *error = nil;
    }
}